﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmInsert tela = new frmInsert();
            tela.Show();
            

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //abre tela consultar
            Telas.frmConsultar tela = new frmConsultar();
            tela.Show();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            // fecha e aplicação
            Application.Exit();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            //abre tela menu de login
            Telas.frmMenuLogin telas = new frmMenuLogin();
            telas.Show();
            this.Hide();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            Telas.frmEmprestimoDevolução tela = new frmEmprestimoDevolução();
            tela.Show();
            this.Hide();
        }
    }
}
