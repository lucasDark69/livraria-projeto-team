﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmEmprestimoDevolução : Form
    {
        public frmEmprestimoDevolução()
        {
            InitializeComponent();
            this.CarregarCombo();
        }


        private void CarregarCombo()
        {
            // faz a consulta no banco de dados 
            DataBase.databaselibrary db = new DataBase.databaselibrary();
            List<DataBase.model.tb_livros> livros = db.Listar();

            // carrega as informações no combo 
            cbolivros.DisplayMember = nameof(DataBase.model.tb_livros.nm_livro);
            cbolivros.DataSource = livros;

        }
        private void Button1_Click(object sender, EventArgs e)
        {

            DataBase.model.tb_livros livro = cbolivros.SelectedItem as DataBase.model.tb_livros;
            int idlivro = livro.id_livro;



            DataBase.model.tb_emprestimo em = new DataBase.model.tb_emprestimo();

            em.dt_emprestimo = dtpdevoloção.Value.Date;
            em.id_livro = idlivro;
            em.id_usuario = Convert.ToInt32( nudiduser.Value);

            //desculpa aai proff <3, nao vou fazer mais

            DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
            DataBase.model.tb_livros mudarDisponibilidade = db.tb_livros.First(t => t.id_livro == idlivro);
            mudarDisponibilidade.bt_emprestado = true;
            db.SaveChanges();

            Business.business busi = new Business.business();
            busi.emprestimo(em);

            MessageBox.Show("sucesso", "Obrigado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DataBase.model.tb_livros livro = cbolivros.SelectedItem as DataBase.model.tb_livros;
            int idlivro = livro.id_livro;
            bool livroEnprestadoOuNão = livro.bt_emprestado;


            //só mais essa
            DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
            DataBase.model.tb_livros mudarDisponibilidade = db.tb_livros.First(t => t.id_livro == idlivro);
            mudarDisponibilidade.bt_emprestado = false;
            db.SaveChanges();



            DataBase.model.tb_emprestimo em = new DataBase.model.tb_emprestimo();
            em.dt_devolucao = dtpemprestimo.Value.Date;
            em.id_emprestimo = Convert.ToInt32(nudIDemprestimo.Value);

            Business.business busi = new Business.business();
            busi.alterar(em);

            MessageBox.Show("sucesso", "Obrigado por devolver", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void Clickbut_Click(object sender, EventArgs e)
        {
            DataBase.model.libraryEntities db = new DataBase.model.libraryEntities();
            List<DataBase.model.tb_livros> list = db.tb_livros.Where(t => t.bt_emprestado == true).ToList();

            gridlist.DataSource = list;
        }
    }
}
