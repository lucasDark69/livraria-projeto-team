﻿namespace BibliotecaAluguel.Telas
{
    partial class frmEmprestimoDevolução
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cbolivros = new System.Windows.Forms.ComboBox();
            this.dtpdevoloção = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.nudiduser = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nudIDemprestimo = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpemprestimo = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.clickbut = new System.Windows.Forms.Button();
            this.gridlist = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.nudiduser)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDemprestimo)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridlist)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Data Emprestimo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Livros";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(185, 171);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 56);
            this.button1.TabIndex = 10;
            this.button1.Text = "pegar o livro";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // cbolivros
            // 
            this.cbolivros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbolivros.FormattingEnabled = true;
            this.cbolivros.Location = new System.Drawing.Point(117, 26);
            this.cbolivros.Name = "cbolivros";
            this.cbolivros.Size = new System.Drawing.Size(168, 21);
            this.cbolivros.TabIndex = 12;
            // 
            // dtpdevoloção
            // 
            this.dtpdevoloção.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdevoloção.Location = new System.Drawing.Point(117, 118);
            this.dtpdevoloção.Name = "dtpdevoloção";
            this.dtpdevoloção.Size = new System.Drawing.Size(168, 20);
            this.dtpdevoloção.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "ID Cliente";
            // 
            // nudiduser
            // 
            this.nudiduser.Location = new System.Drawing.Point(117, 74);
            this.nudiduser.Name = "nudiduser";
            this.nudiduser.Size = new System.Drawing.Size(168, 20);
            this.nudiduser.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudiduser);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cbolivros);
            this.groupBox1.Controls.Add(this.dtpdevoloção);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 254);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "emprestimo de livro";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nudIDemprestimo);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dtpemprestimo);
            this.groupBox2.Location = new System.Drawing.Point(472, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 254);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "emprestimo";
            // 
            // nudIDemprestimo
            // 
            this.nudIDemprestimo.Location = new System.Drawing.Point(110, 53);
            this.nudIDemprestimo.Name = "nudIDemprestimo";
            this.nudIDemprestimo.Size = new System.Drawing.Size(168, 20);
            this.nudIDemprestimo.TabIndex = 9;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(123, 171);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 56);
            this.button2.TabIndex = 9;
            this.button2.Text = "devolver livro";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Data Devolução";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "ID emprestimo";
            // 
            // dtpemprestimo
            // 
            this.dtpemprestimo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpemprestimo.Location = new System.Drawing.Point(110, 96);
            this.dtpemprestimo.Name = "dtpemprestimo";
            this.dtpemprestimo.Size = new System.Drawing.Size(168, 20);
            this.dtpemprestimo.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.clickbut);
            this.groupBox3.Controls.Add(this.gridlist);
            this.groupBox3.Location = new System.Drawing.Point(91, 300);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(644, 288);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "consultar emprestimos";
            // 
            // clickbut
            // 
            this.clickbut.Location = new System.Drawing.Point(346, 232);
            this.clickbut.Name = "clickbut";
            this.clickbut.Size = new System.Drawing.Size(258, 34);
            this.clickbut.TabIndex = 8;
            this.clickbut.Text = "consultar livros emprestados";
            this.clickbut.UseVisualStyleBackColor = true;
            this.clickbut.Click += new System.EventHandler(this.Clickbut_Click);
            // 
            // gridlist
            // 
            this.gridlist.AllowUserToAddRows = false;
            this.gridlist.AllowUserToDeleteRows = false;
            this.gridlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridlist.Location = new System.Drawing.Point(34, 41);
            this.gridlist.Name = "gridlist";
            this.gridlist.ReadOnly = true;
            this.gridlist.Size = new System.Drawing.Size(570, 170);
            this.gridlist.TabIndex = 0;
            // 
            // frmEmprestimoDevolução
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 600);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmEmprestimoDevolução";
            this.Text = "frmEmprestimoDevolução";
            ((System.ComponentModel.ISupportInitialize)(this.nudiduser)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDemprestimo)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridlist)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbolivros;
        private System.Windows.Forms.DateTimePicker dtpdevoloção;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudiduser;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nudIDemprestimo;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpemprestimo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button clickbut;
        private System.Windows.Forms.DataGridView gridlist;
    }
}